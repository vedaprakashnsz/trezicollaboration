package com.trezi.project.domain;

import com.microsoft.azure.spring.data.documentdb.core.mapping.Document;
import org.springframework.data.annotation.Id;

@Document(collection = "project_users")
public class ProjectUsers {

    @Id
    String id;
    String projectName;
    String projectId;
    String username;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public ProjectUsers(String projectName, String username){
        this.projectName = projectName;
        this.username = username;
    }
    public ProjectUsers(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
