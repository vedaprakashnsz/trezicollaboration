package com.trezi.project.domain;

import com.microsoft.azure.spring.data.documentdb.core.mapping.Document;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Document(collection = "project")
@Data
@AllArgsConstructor
public class Project {

    @Id
    String id;
    String name;
    String modelName;
    String organization;
    Boolean isOnline;
    String createdBy;
    String modelLink;
    String snapshotLink;
    Long createdOn;
    Long fileSize;

    Long editedOn;

    public Long getEditedOn() {
        return editedOn;
    }

    public void setEditedOn(Long editedOn) {
        this.editedOn = editedOn;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Long createdOn) {
        this.createdOn = createdOn;
    }

    public String getSnapshotLink() {
        return snapshotLink;
    }

    public void setSnapshotLink(String snapshotLink) {
        this.snapshotLink = snapshotLink;
    }

    //    String _rid;
//    String _self;
//    Long _ts;
//    String _etag;
//    String _attachments;

    /*public String get_rid() {
        return _rid;
    }

    public String get_self() {
        return _self;
    }

    public Long get_ts() {
        return _ts;
    }

    public String get_etag() {
        return _etag;
    }

    public String get_attachments() {
        return _attachments;
    }

    public void set_attachments(String _attachments) {
        this._attachments = _attachments;
    }

    public void set_rid(String _rid) {
        this._rid = _rid;
    }

    public void set_self(String _self) {
        this._self = _self;
    }

    public void set_ts(Long _ts) {
        this._ts = _ts;
    }

    public void set_etag(String _etag) {
        this._etag = _etag;
    }*/

    public String getId() {
        return id;
    }

    public String getModelLink() {
        return modelLink;
    }

    public void setModelLink(String modelLink) {
        this.modelLink = modelLink;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Project(String id,String name, String modelName, String organization, Boolean isOnline, String createdBy){
        this.id = id;
        this.createdBy = createdBy;
        this.isOnline = isOnline;
        this.modelName = modelName;
        this.name = name;
        this.organization = organization;
    }
    public Project(String id,String name, String modelName, String organization, Boolean isOnline, String createdBy, String modelLink){
        this.id = id;
        this.createdBy = createdBy;
        this.isOnline = isOnline;
        this.modelName = modelName;
        this.name = name;
        this.organization = organization;
        this.modelLink = modelLink;
    }

    public Project(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Boolean getOnline() {
        return isOnline;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
