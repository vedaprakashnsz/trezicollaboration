package com.trezi.project.domain;

import com.microsoft.azure.spring.data.documentdb.core.mapping.Document;
import org.springframework.data.annotation.Id;


@Document(collection = "organization")
public class Organization {

    @Id
//    @SequenceGenerator(sequenceName = "APPLICATION_INFO_SEQ", allocationSize = 1, name = "APPLICATION_INFO_SEQ")
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APPLICATION_INFO_SEQ")
    private String id;
    private String name;
    private String address;
    private String pincode;
    private String website;
    private String contactNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Organization(String name, String address, String pincode, String website, String contactNumber){
        this.address = address;
        this.name = name;
        this.contactNumber = contactNumber;
        this.website = website;
        this.pincode = pincode;
    }

}
