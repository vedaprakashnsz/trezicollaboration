package com.trezi.project.dto;

import org.springframework.web.multipart.MultipartFile;

public class ProjectCreationDTO {

    String projectName;
    String createdBy;
    MultipartFile modelFile;
    String projectId;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public MultipartFile getModelFile() {
        return modelFile;
    }

    public void setModelFile(MultipartFile modelFile) {
        this.modelFile = modelFile;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
