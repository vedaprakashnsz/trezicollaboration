package com.trezi.project.dto;

import org.springframework.web.multipart.MultipartFile;

public class ProjectModelUploadDTO {
    String projectId;
    String createdBy;
    MultipartFile modelFile;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public MultipartFile getModelFile() {
        return modelFile;
    }

    public void setModelFile(MultipartFile modelFile) {
        this.modelFile = modelFile;
    }
}
