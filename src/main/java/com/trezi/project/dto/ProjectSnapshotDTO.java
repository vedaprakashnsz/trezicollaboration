package com.trezi.project.dto;

import org.springframework.web.multipart.MultipartFile;

public class ProjectSnapshotDTO {

    String projectId;
    String createdBy;
    MultipartFile snapshotFile;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public MultipartFile getSnapshotFile() {
        return snapshotFile;
    }

    public void setSnapshotFile(MultipartFile snapshotFile) {
        this.snapshotFile = snapshotFile;
    }
}
