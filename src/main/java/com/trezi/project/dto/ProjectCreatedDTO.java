package com.trezi.project.dto;

public class ProjectCreatedDTO {
    String id;
    Long created;
    String createdBy;

    Long edited;

    public Long getEdited() {
        return edited;
    }

    public void setEdited(Long edited) {
        this.edited = edited;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }
}
