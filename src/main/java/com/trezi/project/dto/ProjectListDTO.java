package com.trezi.project.dto;

import java.util.List;

public class ProjectListDTO {

    String name;
    String createdBy;
    List<String> sharedUsers;
    String projectType;
    String userNameLabel;

    Long fileSize;


    String path;
    String id;
    String snapshotLink;
    Long created;

    Long edited;

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getUserNameLabel() {
        return userNameLabel;
    }

    public void setUserNameLabel(String userNameLabel) {
        this.userNameLabel = userNameLabel;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getEdited() {
        return edited;
    }

    public void setEdited(Long edited) {
        this.edited = edited;
    }

    public String getSnapshotLink() {
        return snapshotLink;
    }

    public void setSnapshotLink(String snapshotLink) {
        this.snapshotLink = snapshotLink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<String> getSharedUsers() {
        return sharedUsers;
    }

    public void setSharedUsers(List<String> sharedUsers) {
        this.sharedUsers = sharedUsers;
    }
}
