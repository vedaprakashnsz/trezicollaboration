package com.trezi.project.services;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.*;
import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.MongoClient;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

@Service
public class UtilityService {
    private static final Logger logger = LoggerFactory.getLogger(UtilityService.class.getName());

    public CloudBlobContainer connectToAzureBlobContainer(String azureAccount, String azurePassword, String azureContainer){
        CloudBlobContainer cloudBlobContainer = null;
        try{
            String storageConnectionString =
                    "DefaultEndpointsProtocol=https;" +
                            "AccountName=" + azureAccount + ";" +
                            "AccountKey=" + azurePassword;
            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(storageConnectionString);
            CloudBlobClient cloudBlobClient = cloudStorageAccount.createCloudBlobClient();
            cloudBlobContainer = cloudBlobClient.getContainerReference(azureContainer);
        }
        catch (Exception e){
            logger.error(e.getMessage());
        }
        return cloudBlobContainer;
    }

//    public String uploadFileToContainer(String azureAccount, String azurePassword, String azureContainer, String fileName, MultipartFile file){
//        String modelLink = null;
//        try{
//            CloudBlobContainer cloudBlobContainer = connectToAzureBlobContainer(azureAccount,azurePassword,azureContainer);
//            if(null != cloudBlobContainer){
//                CloudBlockBlob blob = cloudBlobContainer.getBlockBlobReference(fileName);
//                blob.upload(file.getInputStream(), file.getSize());
//
//                modelLink = blob.getUri().getPath();
//                System.out.println("the file name is : " + modelLink);
//            }
//        }
//        catch(Exception e){
//            logger.error(e.getMessage());
//        }
//        return modelLink;
//    }

    public String uploadFileToContainer(String azureAccount, String azurePassword, String azureContainer, String fileName, MultipartFile file){
        String blobUrl = null;
        try{
            CloudBlobContainer cloudBlobContainer = connectToAzureBlobContainer(azureAccount,azurePassword,azureContainer);
            if(null != cloudBlobContainer){
                CloudBlockBlob blob = cloudBlobContainer.getBlockBlobReference(fileName);
                blob.upload(file.getInputStream(), file.getSize());
                String policyName = "Policy_"+file.getOriginalFilename();
                String sasToken = generateSASToken(cloudBlobContainer);
//                String fullUrl = blob.getUri() + "?" + sasToken;
//                        createStoredAccessPolicy(policyName, cloudBlobContainer);
                blobUrl = blob.getUri() + "?" + sasToken;
            }
        }
        catch(Exception e){
            logger.error(e.getMessage());
        }
        return blobUrl;
    }

//    public void createStoredAccessPolicy(String policyName, CloudBlobContainer cloudBlobContainer)
//    {
//        try{
//            SharedAccessBlobPolicy storedPolicy = new SharedAccessBlobPolicy();
//            LocalDateTime now = LocalDateTime.now();
//            Instant result = now.minusHours(24).atZone(ZoneOffset.UTC).toInstant();
//            Date startTime = Date.from(result);
//
//            now = LocalDateTime.now();
//            result = now.plusDays(365).atZone(ZoneOffset.UTC).toInstant();
//            Date expirationTime = Date.from(result);
//            storedPolicy.setSharedAccessStartTime(startTime);
//            storedPolicy.setSharedAccessExpiryTime(expirationTime);
//            storedPolicy.setPermissions(EnumSet.of(SharedAccessBlobPermissions.READ,SharedAccessBlobPermissions.LIST));
//            BlobContainerPermissions permissions = new BlobContainerPermissions();
//
//            permissions.getSharedAccessPolicies().clear();
//            permissions.getSharedAccessPolicies().put(policyName,storedPolicy);
//            cloudBlobContainer.uploadPermissions(permissions);
//        }
//        catch (Exception e){
//            logger.error(e.getMessage());
//        }
//
//    }

    public String generateSASToken(CloudBlobContainer cloudBlobContainer){
        String sas = null;
        try{
            // Create a new shared access policy.
            SharedAccessBlobPolicy sasPolicy = new SharedAccessBlobPolicy();

            // Create a UTC Gregorian calendar value.
            GregorianCalendar calendar = new GregorianCalendar(
                    TimeZone.getTimeZone("UTC"));

            // Specify the current time as the start time for the shared access
            // signature.
            //
            calendar.setTime(new Date());
            sasPolicy.setSharedAccessStartTime(calendar.getTime());

            // Use the start time delta one hour as the end time for the shared
            // access signature.
            calendar.add(Calendar.YEAR, 1);
            sasPolicy.setSharedAccessExpiryTime(calendar.getTime());


            // Set READ permissions
            sasPolicy.setPermissions(EnumSet.of(
                    SharedAccessBlobPermissions.READ,
                    SharedAccessBlobPermissions.LIST));


            // Create the container permissions.
            BlobContainerPermissions containerPermissions = new BlobContainerPermissions();

            // Turn public access to the container off.
            containerPermissions.setPublicAccess(BlobContainerPublicAccessType.OFF);

            cloudBlobContainer.uploadPermissions(containerPermissions);

            // Create a shared access signature for the container.
            sas = cloudBlobContainer.generateSharedAccessSignature(sasPolicy, null);
            // HACK: when the just generated SAS is used straight away, we get an
            // authorization error intermittently. Sleeping for 1.5 seconds fixes that
            // on my box.
            Thread.sleep(1500);

            // Return to caller with the shared access signature.
            return sas;
        }
        catch (Exception e){
            logger.error(e.getMessage());
        }
        return sas;
    }

    public Boolean deleteFiles(String azureAccount, String azurePassword, String azureContainer, String projectName){
        try {
            CloudBlobContainer cloudBlobContainer = connectToAzureBlobContainer(azureAccount, azurePassword, azureContainer);
            if (null != cloudBlobContainer) {
                Iterable<ListBlobItem> blobItems = cloudBlobContainer.listBlobs(projectName,true);
               for(ListBlobItem blob : blobItems){
                   if( null != blob){
//                       cloudBlobContainer.getBlockBlobReference(((CloudBlockBlob)blob).getName()).deleteIfExists();
                       String blobName = blobNameFromUri(blob.getUri());
                       deleteBlob(cloudBlobContainer, blobName);
                   }
               }
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
        }
        return true;
    }

    public static String blobNameFromUri(URI uri) {
        String path = uri.getPath();

        // We remove the container name from the path
        // The 3 magic number cames from the fact if path is /container/path/to/myfile
        // First occurrence is empty "/"
        // Second occurrence is "container
        // Last part contains "path/to/myfile" which is what we want to get
        String[] splits = path.split("/", 3);

        // We return the remaining end of the string
        return splits[2];
    }

    public void deleteBlob(CloudBlobContainer blobContainer, String blob) throws URISyntaxException, StorageException {
        if (blobContainer.exists()) {
            CloudBlockBlob azureBlob = blobContainer.getBlockBlobReference(blob);
            azureBlob.deleteIfExists();
        }
    }

    public Map fetchUserDataFromMongo(){
        String name = null;
        Map<String, String> usersMap = new HashMap<>();
        MongoClient mongoClient = MongoClients.create("mongodb://trezidb-prod:CuWOOwzH4llFJ21wXXZh19GgYH3BAPJ4dGWpBMoqytwpkteC8ZeiFs4edv0U0he8zs9hsYMRUfNJjNjj7HqItw==@trezidb-prod.documents.azure.com:10255/trezi-mongo?ssl=true&replicaSet=globaldb");
        MongoDatabase database = mongoClient.getDatabase("trezi-mongo");
        MongoCollection<Document> mongoCollection = database.getCollection("users");
        for (Document doc : mongoCollection.find()) {
            if(doc != null){
                usersMap.put(doc.get("email").toString(), doc.get("fName") + " " + doc.get("lName"));
            }
            else{
                usersMap.put(doc.get("email").toString(),"NA");
            }
        }
        return usersMap;
    }


    public Map loadData(){
        String name = null;
        Map<String, String> usersMap = new HashMap<>();
        MongoClient mongoClient = MongoClients.create("mongodb://trezidb-prod:CuWOOwzH4llFJ21wXXZh19GgYH3BAPJ4dGWpBMoqytwpkteC8ZeiFs4edv0U0he8zs9hsYMRUfNJjNjj7HqItw==@trezidb-prod.documents.azure.com:10255/trezi-mongo?ssl=true&replicaSet=globaldb");
        MongoDatabase database = mongoClient.getDatabase("trezi-mongo");
        MongoCollection<Document> mongoCollection = database.getCollection("usersnew");
        DBObject dbObject = (DBObject) com.mongodb.util.JSON.parse("C:/Users/vedaprakash/Desktop/users.json");
        List<DBObject> listObject = new ArrayList<>();
        listObject.add(dbObject);
//        mongoCollection.;
        for (Document doc : mongoCollection.find()) {
            if(doc != null){
                usersMap.put(doc.get("email").toString(), doc.get("fName") + " " + doc.get("lName"));
            }
            else{
                usersMap.put(doc.get("email").toString(),"NA");
            }
        }
        return usersMap;
    }
}
