package com.trezi.project.services;


import com.mongodb.DBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CacheService {

public Map<String,String> usersMap = new HashMap<>();

    @PostConstruct
    public void load(){
        MongoClient mongoClient = MongoClients.create("mongodb://trezidb-prod:CuWOOwzH4llFJ21wXXZh19GgYH3BAPJ4dGWpBMoqytwpkteC8ZeiFs4edv0U0he8zs9hsYMRUfNJjNjj7HqItw==@trezidb-prod.documents.azure.com:10255/trezi-mongo?ssl=true&replicaSet=globaldb");
        MongoDatabase database = mongoClient.getDatabase("trezi-mongo");
        MongoCollection<Document> mongoCollection = database.getCollection("users");
        this.usersMap.clear();
        for (Document doc : mongoCollection.find()) {
            if(doc != null){
                usersMap.put(doc.get("email").toString(), doc.get("fName") + " " + doc.get("lName"));
            }
            else{
                usersMap.put(doc.get("email").toString(),"NA");
            }
        }
    }

    public String getUserData(String email){
        if(this.usersMap.keySet().contains(email)){
            return this.usersMap.get(email);
        }
        load();
        return this.usersMap.get(email);
    }

    public Map getUsersMap(){
        return this.usersMap;
    }
}
