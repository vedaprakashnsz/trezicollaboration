package com.trezi.project.services;


import com.trezi.project.domain.Organization;
import com.trezi.project.dto.OrganizationDTO;
import com.trezi.project.repository.OrganizationRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class OrganizationService {

    @Inject
    OrganizationRepository organizationRepository;

    public void createOrganization(OrganizationDTO organizationDTO){
        try{
            Organization organization = new Organization(organizationDTO.getName(),organizationDTO.getAddress(),
                    organizationDTO.getPincode(),organizationDTO.getWebsite(),organizationDTO.getContactNumber());
            organizationRepository.save(organization);
        }
        catch (Exception e){

        }

    }
}
