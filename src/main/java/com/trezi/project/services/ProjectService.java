package com.trezi.project.services;

//import com.trezi.project.config.ExternalConfig;
import com.trezi.project.domain.Project;
import com.trezi.project.domain.ProjectUsers;
import com.trezi.project.dto.*;
import com.trezi.project.repository.ProjectRepository;
import com.trezi.project.repository.ProjectUsersRepository;
import com.trezi.project.utility.Constants;
import exception.ProjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ProjectService {

    @Value(("${azure.account.name}"))
    private String azureAccountName;

    @Value(("${azure.account.pswd}"))
    private String azurePassword;

    @Value(("${azure.container}"))
    private String azureContainer;

    @Value(("${deploy.environment}"))
    private String deploymentEnvironment;

    @Inject
    UtilityService utilityService;

    @Inject
    ProjectRepository projectRepository;

    @Inject
    CacheService cacheService;

    @Inject
    ProjectUsersRepository projectUsersRepository;

    private static final Logger logger = LoggerFactory.getLogger(ProjectService.class.getName());

    public ProjectListDTO upload(ProjectModelUploadDTO projectModelUploadDTO){
        try{
            Project project = null;
            if(projectModelUploadDTO.getProjectId() != null){
                project = projectRepository.findById(projectModelUploadDTO.getProjectId()).get();
            }
            Map<String, String> usersMap = cacheService.getUsersMap();
            project.setModelName(projectModelUploadDTO.getModelFile().getName());
            String modelLink = utilityService.uploadFileToContainer(this.azureAccountName, this.azurePassword, this.azureContainer,
                    "default/" + project.getName() + "/" + "model/" + projectModelUploadDTO.getModelFile().getOriginalFilename(),projectModelUploadDTO.getModelFile());
            project.setCreatedOn((new Date()).getTime());
            project.setModelLink(modelLink);
            project.setEditedOn(((new Date()).getTime()));
            project.setModelName(projectModelUploadDTO.getModelFile().getName());
            project.setFileSize(projectModelUploadDTO.getModelFile().getSize());
            projectRepository.save(project);


            ProjectListDTO projectListDTO = new ProjectListDTO();
            Boolean isOwnedOrShared = true;
            projectListDTO.setId(project.getId());
            projectListDTO.setCreatedBy(project.getCreatedBy());
            projectListDTO.setProjectType(Constants.OWNED);
            projectListDTO.setName(project.getName());
            projectListDTO.setPath(project.getModelLink());
            projectListDTO.setSnapshotLink(project.getSnapshotLink());
            projectListDTO.setCreated(project.getCreatedOn());
            if(project.getFileSize() > 0 ) {
                projectListDTO.setFileSize(project.getFileSize());
            }
            else{
                projectListDTO.setFileSize(new Long(0));
            }
            List<ProjectUsers> projectUsers = projectUsersRepository.findAllByProjectId(project.getId());
            String userDetails = (usersMap.keySet().contains(project.getCreatedBy())) ? usersMap.get(project.getCreatedBy()) : cacheService.getUserData(project.getCreatedBy());
            projectListDTO.setUserNameLabel(userDetails);
            return projectListDTO;
        }
        catch (Exception e){
            logger.error(e.getMessage());
            throw new ProjectException(e);
        }
    }

    public ProjectCreatedDTO create(ProjectCreationDTO projectCreationDTO){
        ProjectCreatedDTO projectCreatedDTO = new ProjectCreatedDTO();
        try{
            Project project = (null != projectRepository.findByName(projectCreationDTO.getProjectName()) && ((List)projectRepository.findByName(projectCreationDTO.getProjectName())).size() > 0)
                    ? (Project)((List)projectRepository.findByName(projectCreationDTO.getProjectName())).get(0)
                    :null ;
            if(null != project) throw new ProjectException("A Project with the given name exists");
            Project createdProject = new Project(null,projectCreationDTO.getProjectName(),null,"default", true,projectCreationDTO.getCreatedBy());
            createdProject.setCreatedOn((new Date()).getTime());
            createdProject.setEditedOn((new Date()).getTime());
            createdProject = projectRepository.save(createdProject);
            createdProject = (null != projectRepository.findByName(projectCreationDTO.getProjectName()) && ((List)projectRepository.findByName(projectCreationDTO.getProjectName())).size() > 0)
                    ? (Project)((List)projectRepository.findByName(projectCreationDTO.getProjectName())).get(0)
                    :null ;
            projectCreatedDTO.setCreated(createdProject.getCreatedOn());
            projectCreatedDTO.setEdited(createdProject.getEditedOn());
            projectCreatedDTO.setId(createdProject.getId());
            projectCreatedDTO.setCreatedBy(createdProject.getCreatedBy());
        }
        catch (Exception e){
            logger.error(e.getMessage());
            throw new ProjectException(e);
        }

        return projectCreatedDTO;
    }

    public List<ProjectListDTO> getList(String userid){
        List<ProjectListDTO> projectsList = new ArrayList();
        try{

            Iterable<Project> projects = projectRepository.findAll();
            Map<String, String> usersMap = cacheService.getUsersMap();
            for(Project project : projects){
                Boolean isOwnedOrShared = false;
                ProjectListDTO projectListDTO = new ProjectListDTO();
                projectListDTO.setId(project.getId());
                projectListDTO.setCreatedBy(project.getCreatedBy());
                if(project.getCreatedBy().equalsIgnoreCase(userid)){
                    isOwnedOrShared = true;
                    projectListDTO.setProjectType(Constants.OWNED);
                }
                projectListDTO.setName(project.getName());
                projectListDTO.setPath(project.getModelLink());
                projectListDTO.setSnapshotLink(project.getSnapshotLink());
                projectListDTO.setCreated(project.getCreatedOn());
                if(project.getModelLink() != null && project.getFileSize() != null && project.getFileSize() > 0 ) {
                    projectListDTO.setFileSize(project.getFileSize());
                }
                else{
                    projectListDTO.setFileSize(new Long(0));
                }
                List<ProjectUsers> projectUsers = projectUsersRepository.findAllByProjectId(project.getId());
                if(null != projectUsers && projectUsers.size() > 0){
                    List<String> projectUserEmails = new ArrayList<>();
                    for(ProjectUsers sharedEmail : projectUsers){
                        if(userid.equalsIgnoreCase(sharedEmail.getUsername())){
                            projectListDTO.setProjectType(Constants.SHARED);
                            isOwnedOrShared = true;
                        }
                        projectUserEmails.add(sharedEmail.getUsername());
                    }
                    projectListDTO.setSharedUsers(projectUserEmails);
                }
                if(isOwnedOrShared){
                    String userDetails = (usersMap.keySet().contains(userid)) ? usersMap.get(userid) : cacheService.getUserData(userid);
                    projectListDTO.setUserNameLabel(userDetails);
                    projectsList.add(projectListDTO);
                }
            }
        }
        catch (Exception e){
            logger.error("Error fetching projects list " + e.getMessage());
            throw new ProjectException(e.getMessage());
        }
        return projectsList;
    }

    public String uploadSnapshotFile(ProjectSnapshotDTO projectSnapshotDTO){
        try {
            Project project = (projectRepository.findById(projectSnapshotDTO.getProjectId())).get();
            if(null == project) throw new ProjectException("No Project with the given details exists");
            String snapShotLink = utilityService.uploadFileToContainer(this.azureAccountName, this.azurePassword, this.azureContainer,
                    "default/" + project.getName() + "/" + "snapshot/" + projectSnapshotDTO.getSnapshotFile().getOriginalFilename(),projectSnapshotDTO.getSnapshotFile());
            project.setSnapshotLink(snapShotLink);
            projectRepository.save(project);
            return snapShotLink;
        }
        catch (Exception e){
            logger.error(e.getMessage());
            throw new ProjectException(e);
        }
    }

    public Boolean shareProjectWithUsers(ProjectSharingDTO projectSharingDTO){
        try{
            List<String> sharedUsers = projectSharingDTO.getSharedUsers();
            if(null == sharedUsers || sharedUsers.size() == 0) throw new ProjectException("Shared Users List cannot be empty");
            Project project = (projectRepository.findById(projectSharingDTO.getProjectId())).get();
            List<ProjectUsers> existingProjectUsers = projectUsersRepository.findAllByProjectId(project.getId());
            for(ProjectUsers existingUser : existingProjectUsers){
                projectUsersRepository.delete(existingUser);
            }
            for(String sharedUser : sharedUsers){
                ProjectUsers projectUsers = new ProjectUsers();
                projectUsers.setProjectName(project.getName());
                projectUsers.setUsername(sharedUser);
                projectUsers.setProjectId(project.getId());
                projectUsersRepository.save(projectUsers);
            }
        }
        catch (Exception e){
            logger.error("Error sharing project with users " + e.getMessage());
            throw new ProjectException("Error sharing project with users");
        }
        return true;
    }

    public Boolean deleteProject(String projectId,
                                 String userid){

            Project project = projectRepository.findById(projectId).get();
            if (null == project) throw new ProjectException("No Project with the provided details exists.");
            if (null != project) {
            if (!project.getCreatedBy().equalsIgnoreCase(userid))
                throw new ProjectException("You do not have privileges to delete this project.");
            try {
                utilityService.deleteFiles(this.azureAccountName, this.azurePassword, this.azureContainer, "default/" + project.getName());
                projectRepository.delete(project);
                projectUsersRepository.deleteAllByProjectId(projectId);
            }
            catch (Exception e){
                logger.error("Error deleting projects " + e.getMessage());
                throw new ProjectException("Error deleting projects ");
            }
        }

        return true;
    }
}
