package com.trezi.project.controllers;

import com.trezi.project.dto.OrganizationDTO;
import com.trezi.project.services.OrganizationService;
import com.trezi.project.utility.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/organization")
public class OrganizationController {

    @Inject
    RestResponse restResponse;

    @Inject
    OrganizationService organizationService;

    private static final Logger logger = LoggerFactory.getLogger(OrganizationController.class.getName());

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getOrganizationList() {
        System.out.println("in app1");
        return restResponse.success("List sent");
    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> createOrganization(OrganizationDTO organizationDTO) {
        System.out.println("in organization create");
        organizationService.createOrganization(organizationDTO);
        return restResponse.success("List sent");
    }
}
