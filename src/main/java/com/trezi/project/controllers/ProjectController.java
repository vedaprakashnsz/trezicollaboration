package com.trezi.project.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trezi.project.dto.*;
import com.trezi.project.services.ProjectService;
import com.trezi.project.services.UtilityService;
import com.trezi.project.utility.RestResponse;
import exception.ProjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ProjectController {
    private static final Logger logger = LoggerFactory.getLogger(ProjectController.class.getName());

    @Inject
    RestResponse restResponse;

    @Inject
    ProjectService projectService;

    @Inject
    UtilityService utilityService;

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getProjectsList(@RequestParam("userid") String userid) {
        try{
//            utilityService.loadData();
            if(userid == null)throw new ProjectException("Userid snot present");
            List<ProjectListDTO> projectDTOS = projectService.getList(userid);
            return restResponse.successWithData(projectDTOS);
        }
        catch (Exception e){
            logger.error(" the error in listing call : " + e.getMessage());
            throw new ProjectException("Listing Call Fails");
        }
    }

    @RequestMapping(value = "/project/uploadModelFile", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> uploadModelFile(@RequestPart("modelFile") MultipartFile modelFile,
                                                  @RequestPart String  projectId,
                                                   @RequestPart String createdBy) {
        ProjectModelUploadDTO projectModelUploadDTO = new ProjectModelUploadDTO();
        projectModelUploadDTO.setModelFile(modelFile);
        projectModelUploadDTO.setProjectId(projectId);
        projectModelUploadDTO.setCreatedBy(createdBy);
        ProjectListDTO projectListDTO = projectService.upload(projectModelUploadDTO);
        ObjectMapper objectMapper = new ObjectMapper();
        Map json = objectMapper.convertValue(projectListDTO, Map.class);
        return restResponse.successWithData(json);
    }



    @RequestMapping(value = "/project/uploadSnapshot", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> uploadSnapshot(@RequestPart String  projectId,
                                    @RequestPart String createdBy,
                                    @RequestPart("snapshot") MultipartFile snapshot) {
        ProjectSnapshotDTO projectSnapshotDTO = new ProjectSnapshotDTO();
        projectSnapshotDTO.setCreatedBy(createdBy);
        projectSnapshotDTO.setProjectId(projectId);
        projectSnapshotDTO.setSnapshotFile(snapshot);
        String snapshotLink = projectService.uploadSnapshotFile(projectSnapshotDTO);
        Map projectMap = new HashMap();
        projectMap.put("snapshotLink", snapshotLink);
        return restResponse.successWithData(projectMap);
    }

    @RequestMapping(value = "/project/share/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> share(@PathVariable String id,
                                            @RequestBody String sharedUsers) {
        ProjectSharingDTO projectSharingDTO = new ProjectSharingDTO();
        projectSharingDTO.setProjectId(id);
        if(null != sharedUsers){
            List<String> sharedUsersList = Arrays.asList(sharedUsers.split(","));
            projectSharingDTO.setSharedUsers(sharedUsersList);
        }
        projectService.shareProjectWithUsers(projectSharingDTO);
        return restResponse.success("Project Shared Successfully");
    }

    @RequestMapping(value = "/project/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable String id,
                                    @RequestParam("userid") String userid) {

        Boolean isDeleted = projectService.deleteProject(id,userid);
        return restResponse.success("Project Deleted.");
    }

    @RequestMapping(value = "/project", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> create(@RequestBody ProjectDTO  projectDTO) {
        ProjectCreationDTO projectCreationDTO = new ProjectCreationDTO();
        projectCreationDTO.setCreatedBy(projectDTO.getCreatedBy());
        projectCreationDTO.setProjectName(projectDTO.getName());
        ProjectCreatedDTO projectCreatedDTO = projectService.create(projectCreationDTO);
        ObjectMapper objectMapper = new ObjectMapper();
        Map json = objectMapper.convertValue(projectCreatedDTO, Map.class);
        return restResponse.successWithData(json);
    }


}
