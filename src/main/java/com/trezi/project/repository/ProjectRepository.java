package com.trezi.project.repository;


import com.microsoft.azure.spring.data.documentdb.repository.DocumentDbRepository;
import com.trezi.project.domain.Project;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ProjectRepository extends DocumentDbRepository<Project, String> {

    List findByName(String name);

}
