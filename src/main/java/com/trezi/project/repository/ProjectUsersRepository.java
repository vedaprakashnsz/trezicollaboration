package com.trezi.project.repository;

import com.microsoft.azure.spring.data.documentdb.repository.DocumentDbRepository;
import com.trezi.project.domain.Project;
import com.trezi.project.domain.ProjectUsers;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectUsersRepository extends DocumentDbRepository<ProjectUsers, String> {
    List<ProjectUsers> findAllByProjectId(String projectId);
     public void deleteAllByProjectId(String projectId);
}
