package com.trezi.project.repository;

import com.microsoft.azure.spring.data.documentdb.repository.DocumentDbRepository;
import com.trezi.project.domain.Organization;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizationRepository extends DocumentDbRepository<Organization, String> {
}
