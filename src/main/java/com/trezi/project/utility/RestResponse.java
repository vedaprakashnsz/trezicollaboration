package com.trezi.project.utility;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class RestResponse {


    public ResponseEntity<?> success(String message){
        Map<String, Object> apiResponse = new HashMap<String, Object>();
        apiResponse.put("data", "{}");
        apiResponse.put("message", message);
        apiResponse.put("status", Constants.RESPONSE_SUCCESS_STATUS);
        apiResponse.put("code", Constants.REQUEST_SUCCESS_CODE);

        return new ResponseEntity(apiResponse, HttpStatus.OK);
    }

    public ResponseEntity<?> successWithData(List dataList){
        Map<String, Object> apiResponse = new HashMap<String, Object>();
        apiResponse.put("data", dataList);
        apiResponse.put("message", "");
        apiResponse.put("status", Constants.RESPONSE_SUCCESS_STATUS);
        apiResponse.put("code", Constants.REQUEST_SUCCESS_CODE);

        return new ResponseEntity(apiResponse, HttpStatus.OK);
    }


    public ResponseEntity<?> successWithData(Map<String, Object> dataMap){
        Map<String, Object> apiResponse = new HashMap<String, Object>();
        apiResponse.put("data", dataMap);
        apiResponse.put("message", "");
        apiResponse.put("status", Constants.RESPONSE_SUCCESS_STATUS);
        apiResponse.put("code", Constants.REQUEST_SUCCESS_CODE);

        return new ResponseEntity(apiResponse, HttpStatus.OK);
    }


    public ResponseEntity<?> successWithDataAndCustomMessage(Map<String, Object> dataMap, String message){
        Map<String, Object> apiResponse = new HashMap<String, Object>();
        apiResponse.put("data", dataMap);
        apiResponse.put("message", message);
        apiResponse.put("status", Constants.RESPONSE_SUCCESS_STATUS);
        apiResponse.put("code", Constants.REQUEST_SUCCESS_CODE);

        return new ResponseEntity(apiResponse, HttpStatus.OK);
    }



    public ResponseEntity<?> successWithCustomStatus(Map<String, Object> dataMap, String message, HttpStatus httpStatus){
        Map<String, Object> apiResponse = new HashMap<String, Object>();
        apiResponse.put("data", dataMap);
        apiResponse.put("message", message);
        apiResponse.put("status", Constants.RESPONSE_SUCCESS_STATUS);
        apiResponse.put("code", Constants.REQUEST_SUCCESS_CODE);

        return new ResponseEntity(apiResponse, httpStatus);
    }


    public ResponseEntity<?> failure(String message){
        Map<String, Object> apiResponse = new HashMap<String, Object>();
        apiResponse.put("data", "{}");
        apiResponse.put("message", message);
        apiResponse.put("status", Constants.RESPONSE_FAILED_STATUS);
        apiResponse.put("code", Constants.REQUEST_FAILURE_CODE);

        return new ResponseEntity(apiResponse, HttpStatus.OK);
    }



    public ResponseEntity<?> failureWithCustomStatus(String message, HttpStatus httpStatus){
        Map<String, Object> apiResponse = new HashMap<String, Object>();
        apiResponse.put("data", "{}");
        apiResponse.put("message", message);
        apiResponse.put("status", Constants.RESPONSE_FAILED_STATUS);
        apiResponse.put("code", Constants.REQUEST_FAILURE_CODE);

        return new ResponseEntity(apiResponse, httpStatus);
    }


    public ResponseEntity<?> failureWithCustomCode(String message, String code){
        Map<String, Object> apiResponse = new HashMap<String, Object>();
        apiResponse.put("data", "{}");
        apiResponse.put("message", message);
        apiResponse.put("status", Constants.RESPONSE_FAILED_STATUS);
        apiResponse.put("code", code);

        return new ResponseEntity(apiResponse, HttpStatus.OK);
    }
}

