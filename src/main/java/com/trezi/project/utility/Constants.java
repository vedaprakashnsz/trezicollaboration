package com.trezi.project.utility;

public class Constants {

    public static final String RESPONSE_SUCCESS_STATUS = "success";
    public static final String RESPONSE_FAILED_STATUS = "failure";
    public static final Integer REQUEST_FAILURE_CODE = 500;
    public static final Integer REQUEST_SUCCESS_CODE = 200;
    public static final Integer UNAUTHORIZED_ACCESS_CODE = 401;
    public static final String OWNED = "owned";
    public static final String SHARED = "shared";
    public static final String CUSTOM_SAML_TOKEN = "CUSTOM!!S@ML!!TOKEN";
}
