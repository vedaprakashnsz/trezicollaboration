package exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

public class ProjectException extends RuntimeException{
    String cleanedMessage;
    HttpStatus returnStatus;

    public String getCleanedMessage() { return cleanedMessage; }

    public ProjectException (String message) {
        super(message);
        returnStatus = HttpStatus.ACCEPTED;
    }

    public ProjectException (String message, HttpStatus  retStatus) {
        super(message);
        returnStatus = retStatus;
    }

    public ProjectException (Throwable throwable){
        super(throwable);

        if (throwable instanceof JsonProcessingException) {
            cleanedMessage = throwable.getMessage();
            returnStatus = HttpStatus.BAD_REQUEST;
        } else if (throwable instanceof RestClientException) {
            if (throwable instanceof HttpStatusCodeException) {
                cleanedMessage = ((HttpStatusCodeException)throwable).getResponseBodyAsString();
                returnStatus = ((HttpStatusCodeException) throwable).getStatusCode();
            } else {
                cleanedMessage = throwable.getMessage();
                returnStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            }
        }
    }

    public HttpStatus getreturnStatus() {
        return returnStatus;
    }
}

